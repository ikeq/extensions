export * from './snippet';
export * from './classnames';
export * from './define';

export function createElement<T extends HTMLElement>(tag: string, props = {} as any, children?: HTMLElement[]): T {
  const el = document.createElement(tag) as T;
  if (props.class) {
    el.classList.add(props.class);
  }
  if (props.props) {
    Object.keys(props.props).forEach(key => el[key] = props.props[key]);
  }
  if (props.innerHTML) {
    el.innerHTML = props.innerHTML;
  }
  if (children) {
    children.forEach(child => {
      el.appendChild(child);
    })
  }
  return el;
}
