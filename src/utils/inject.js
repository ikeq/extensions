export function mapToAttrs(map) {
  const ret = Object.keys(map)
    .reduce((ret, k) => (map[k] || map[k] === 0 ? ret.concat(`${k.replace(/_/g, '-')}="${map[k]}"`) : ret), [])
    .join(' ');

  return ret ? ` ${ret}` : '';
}
