declare module '*.scss' {
  const content: string;
  export default content;
}
declare interface Window {
  __inside__: any;
  DISQUS: any,
  disqus_config: any;
  LivereTower: any;
  livereOptions: any;
  adsbygoogle: any;
}
