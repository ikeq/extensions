import debounce from 'lodash/debounce';
import cryptoJS from '../../vendor/aes';
import { createElement, define, execHtml } from '../../utils';

import raw from './style.scss';
const [style, css] = JSON.parse(raw);

const { AES, enc: { Utf8 } } = cryptoJS;

function decrypt(input, passcode) {
  try {
    return AES.decrypt(input, passcode).toString(Utf8);
  } catch (e) {
    //
  };
}

define('cipher', {
  style,
  connected({ root }) {
    const host = root.host as HTMLElement;

    const placeholder = host.getAttribute('placeholder') || 'Passcode is required';
    const input = createElement<HTMLInputElement>('input', { class: css.field, props: { placeholder } });
    const field = createElement('div', { class: css.field }, [
      input, createElement('span', { innerHTML: placeholder })
    ]);
    const control = createElement('div', { class: css.cipher }, [field]);
    const mask = createElement('div', { class: css.mask });

    input.addEventListener('input', debounce((ev: any) => {
      const decrypted = decrypt(mask.innerHTML, input.value);
      if (decrypted) execHtml(decrypted, host);
    }, 100));

    root.appendChild(control);
    root.appendChild(mask);

    setTimeout(() => {
      mask.innerHTML = host.innerHTML;
      host.innerHTML = '';
      host.style.filter = 'none';
    });
  }
});
