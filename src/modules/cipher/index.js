const container = require(process.cwd() + '/node_modules/markdown-it-container');
import aes from 'crypto-js/aes';

export function exec(hexo, config = {}, helper) {
  hexo.extend.filter.register('after_post_render', (data) => {
    if (!['page', 'post'].includes(data.layout)) return;

    data.content = data.content.replace(
      /__CIPHER\|SRT:(.*?)\|PLR:(.*?)__([\s\S]*?)__CIPHEREND__/g, (_, $1, $2, $3) => {
        return `<is-cipher placeholder="${$2}">${aes.encrypt($3, $1)}</is-cipher>`;
      }
    );

    if (data.secret) {
      const {
        placeholder = config.placeholder,
        excerpt = config.excerpt
      } = data.cipher || {};
      data.content = `<is-cipher placeholder="${placeholder}">${aes.encrypt(data.content, String(data.secret))}</is-cipher>`

      if (data.toc) {
        delete data.toc
      }
      if (data.excerpt) {
        data.excerpt = excerpt
      }
    }
  }, 11);
  hexo.extend.filter.register('inside:renderer', function(renderer) {
    renderer.use(container, 'cipher', {
      render(tokens, idx, options, env, slf) {
        const meta = tokens[idx].info.trim().slice(7).split(' ');
        const secret = meta[0].trim();
        const placeholder = (meta[1] || '').trim();

        return tokens[idx].nesting === 1
          ? `__CIPHER|SRT:${secret}|PLR:${placeholder}__`
          : '__CIPHEREND__';
      }
    });
  });
  /* replacement generator */
}

export const schema = {
  properties: {
    placeholder: { type: 'string', default: 'Passcode is required' },
    excerpt: { type: 'string', default: 'Content encrypted' }
  },
  required: ['placeholder', 'excerpt'],
  additionalProperties: false
}
