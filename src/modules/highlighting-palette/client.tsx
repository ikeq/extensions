/** @jsx h */
import { h, render, Fragment } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { classnames as $, define } from '../../utils';

import raw from './style.scss';
const [style, css] = JSON.parse(raw);

interface Pattern {
  name: string;
  download_url: string;
}

define('highlighting-palette', {
  style,
  created(el) {
    render(<App />, el);
  }
});

function App() {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<Error>(null);
  const [theme, setTheme] = useState<{ name: string; value: string }>({} as any);
  const [patterns, setPatterns] = useState<Pattern[]>([]);

  useEffect(load, []);

  function load() {
    setLoading(true);
    getAll()
      .then(setPatterns)
      .catch(setError)
      .finally(() => setLoading(false));
  }

  function fetchTheme(pattern: Pattern) {
    setLoading(true);
    getBase16(pattern.download_url)
      .then(colors => {
        const matrix = colors.reduce((tv, cv, i) => {
          cv = `'${cv}'`;
          if (i % 4 === 0) cv = `  ${cv}`;
          return `${tv}${cv}${i < 15 ? ',' : ''}${(i + 1) % 4 === 0 ? '\n' : ' '}`;
        }, '');

        setTheme({
          name: pattern.name,
          value: [`# ${pattern.name}`, `highlight: [`, `${matrix}]`].join('\n'),
        });

        document.dispatchEvent(
          new CustomEvent('inside', {
            detail: {
              type: 'theme',
              data: { highlight: colors },
            },
          })
        );
      })
      .catch(alert)
      .finally(() => setLoading(false));
  }

  return (
    <Fragment>
      <p>
        来源：
        <a href='https://github.com/chriskempson/base16' target='_blank'>@chriskempson/base16</a>
      </p>
      {!loading && error && <p onClick={load} className={css['has-error']}>{error.message}. <a>Click to Retry</a></p>}
      {!patterns.length && loading && <p>Loading...</p>}
      {!!patterns.length && (
        <section className={$(loading && css['is-loading'])}>
          {patterns.map(pattern => (
            <i
              class={$(theme.name === pattern.name && 'highlight')}
              className={$(theme.name === pattern.name && css['is-selected'])}
              onClick={() => fetchTheme(pattern)}
            >
              {pattern.name}
            </i>
          ))}
        </section>
      )}
      {!!patterns.length && <p>拷贝设置到 `appearance.highlight` 应用相应配色。</p>}
      {!!patterns.length && (
        <textarea placeholder='highlight: []' value={theme.value}></textarea>
      )}
    </Fragment>
  );
}

function getAll(): Promise<Pattern[]> {
  const api = 'https://api.github.com/repos/samme/base16-styles/contents/scss';
  const fromLocal = localStorage.getItem(api);

  if (fromLocal) {
    try {
      return Promise.resolve(JSON.parse(fromLocal));
    } catch (error) {
      localStorage.removeItem(api);
    }
  }

  return new Promise((resolve, reject) => {
    fetch(api)
      .then(res => res.json())
      .then(res => {
        if (res.message) {
          reject(res.message);
          return;
        }
        const list = res.map(i => {
          return {
            name: i.name.substring(7).split('.')[0].replace(/-/g, ' '),
            download_url: i.download_url,
          };
        });
        localStorage.setItem(api, JSON.stringify(list));
        resolve(list);
      })
      .catch(reject);
  });
}

function getBase16(url) {
  const fromLocal = localStorage.getItem(url);

  if (fromLocal) {
    try {
      return Promise.resolve(JSON.parse(fromLocal));
    } catch (error) {
      localStorage.removeItem(url);
    }
  }

  return new Promise((resolve, reject) => {
    fetch(url)
      .then(res => res.text())
      .then((res: any) => {
        if (res.message) {
          reject(res.message);
          return;
        }
        return res.split('\n\n')[1];
      })
      .then(base16 => {
        const colors = base16
          .toLowerCase()
          .split('\n')
          .filter(i => i.trim())
          .map(i => '#' + i.split('#')[1].substring(0, 6));

        localStorage.setItem(url, JSON.stringify(colors));
        resolve(colors);
      })
      .catch(reject)
  })
}
