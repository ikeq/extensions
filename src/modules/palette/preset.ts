export default [
  {
    background: '#e2d3b6 url(https://www.toptal.com/designers/subtlepatterns/patterns/vintage-concrete.png)',
    sidebar_background: '#e2d3b6 url(https://www.toptal.com/designers/subtlepatterns/patterns/vintage-concrete.png)',
    card_background: 'url(https://www.toptal.com/designers/subtlepatterns/patterns/paper.png)',
    foreground_color: '#363636',
    border_color: '#fff',
    highlight: [
      '',
      '#fff',
      '#fff',
      '#95a5b3',
      '#363636',
      '#262b2f',
      '#000000',
      '#000000',
      '#5d6c7b',
      '#40464a',
      '#2980b9',
      '#be516e',
      '#237dac',
      '#944770',
      '#239371',
      '#fff',
    ],
    color: ['#e2d3b6', '#e2d3b6'],
  },
  // [accent_color, sidebar_background]
  ...[
    ['#e84701', '#e84701 url(https://cdn.jsdelivr.net/gh/ikeq/inside-static@assets/hodgepodge.jpg)'],
    ['#ff76ab', '#ff76ab linear-gradient(to bottom,#ff76ab 0%,#ff5640 100%)'],
    ['#1a1a1a', '#1a1a1a url(https://cdn.jsdelivr.net/gh/ikeq/inside-static@assets/ocean.jpg)'],
  ].map(i => {
    return {
      accent_color: i[0],
      sidebar_background: i[1],
    };
  }),
  ...['673ab7', '3f51b5', '2196f3', '009688', '4caf50', 'ff9800', 'ff5722', '795548', '607D8B', '2a2b33'].map(i => ({
    accent_color: '#' + i,
    sidebar_background: '#' + i,
  })),
];
