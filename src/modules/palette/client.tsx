/** @jsx h */
import { h, render } from 'preact';
import { classnames as $, define } from '../../utils';
import { useState } from 'preact/hooks';

import raw from './style.scss';
const [style, css] = JSON.parse(raw);

define('palette', {
  style,
  attrs: ['theme', 'col'],
  created(root) {
    return {
      render(props) {
        render(<App {...props} />, root);
      },
      props: {}
    }
  },
  changed({ props, render }, [name, , value]) {
    if (name === 'theme') {
      render(Object.assign(props, { color: value.split(',') }))
    } else if (name === 'col' && !Number.isNaN(+value)) {
      render(Object.assign(props, { col: +value }))
    }
  }
})

function applyTheme(data) {
  document.dispatchEvent(new CustomEvent('inside', {
    detail: {
      type: 'theme',
      data
    }
  }));
}

function App(props: { color: any[], col?: number }) {
  const { color } = props;
  const col = Math.min(color.length, props.col > 0 ? props.col : 5) || 1;
  const [current, setCurrent] = useState<number>(-1);

  return (
    <section className={css.root} style={{ width: `${col * 24 + col * 2}px` }}>
      {color.map((theme, $i) => {
        const style = {
          background: theme,
          margin: '1px'
        };
        return (
          <a style={style} className={$(current === $i && css['is-active'])} title={theme} onClick={() => {
            setCurrent($i); applyTheme({
              accent_color: theme
            })
          }} />
        )
      })}</section>
  );
}
