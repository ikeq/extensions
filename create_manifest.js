const { join } = require('path');
const { readdirSync, writeFileSync, readFileSync, removeSync } = require('fs-extra');

const modules = readdirSync(join(__dirname, './src/modules')).filter((i) => !i.startsWith('.'));
const dist = readdirSync(join(__dirname, './dist')).filter((i) => !i.startsWith('.'));

modules.forEach((name) => {
  const clientName = dist.find((i) => i.startsWith(`${name}.`));
  const clientPath = join(__dirname, 'dist', clientName);
  const distPath = join(__dirname, `dist/${name}.js`);
  const clientCode = readFileSync(clientPath, 'utf8');

  const retCode = readFileSync(distPath, 'utf8')
    .replace(/REPLACEMENT_NAME/g, clientName)
    .replace('REPLACEMENT_CODE', () => JSON.stringify(clientCode).slice(1, -1));

  writeFileSync(distPath, retCode);
  removeSync(clientPath);
});

writeFileSync(
  join(__dirname, `dist/manifest.json`),
  JSON.stringify(modules.reduce((map, m) => ({ ...map, [m]: `${m}.js` }), {}))
);
